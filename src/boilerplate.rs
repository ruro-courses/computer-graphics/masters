use std::iter::once;
use std::sync::Arc;

use vulkano::device::{Device, DeviceExtensions, Queue};
use vulkano::format::Format;
use vulkano::image::view::ImageView;
use vulkano::image::{ImageAccess, ImageUsage, SwapchainImage};
use vulkano::instance::{Instance, PhysicalDevice};
use vulkano::pipeline::vertex::{OneVertexOneInstanceDefinition, SingleBufferDefinition};
use vulkano::pipeline::viewport::Viewport;
use vulkano::pipeline::{GraphicsPipeline, GraphicsPipelineAbstract};
use vulkano::render_pass::{Framebuffer, FramebufferAbstract, RenderPass, Subpass};
use vulkano::swapchain::{
    ColorSpace, FullscreenExclusive, PresentMode, Surface, SurfaceTransform, Swapchain,
};
use vulkano_win::VkSurfaceBuild;
use winit::dpi::PhysicalSize;
use winit::event_loop::EventLoop;
use winit::window::{Window, WindowBuilder};

use crate::make_wd_fn;
use crate::objects;
use crate::shaders;

pub fn make_swapchain(
    physical: PhysicalDevice,
    surface: &Arc<Surface<Window>>,
    device: &Arc<Device>,
    queue: Arc<Queue>,
) -> (Arc<Swapchain<Window>>, Vec<Arc<SwapchainImage<Window>>>) {
    let caps = surface.capabilities(physical).unwrap();
    let format = caps.supported_formats[0].0;
    let dimensions: [u32; 2] = surface.window().inner_size().into();
    let alpha = caps.supported_composite_alpha.iter().next().unwrap();
    Swapchain::start(device.clone(), surface.clone())
        .num_images(caps.min_image_count)
        .format(format)
        .dimensions(dimensions)
        .layers(1)
        .usage(ImageUsage::color_attachment())
        .sharing_mode(&queue)
        .transform(SurfaceTransform::Identity)
        .composite_alpha(alpha)
        .present_mode(PresentMode::Fifo)
        .fullscreen_exclusive(FullscreenExclusive::Default)
        .clipped(true)
        .color_space(ColorSpace::SrgbNonLinear)
        .build()
        .unwrap()
}

pub fn make_physical(
    instance: &Arc<Instance>,
) -> (PhysicalDevice, EventLoop<()>, Arc<Surface<Window>>) {
    let physical = PhysicalDevice::enumerate(&instance).next().unwrap();

    let event_loop = EventLoop::new();
    let surface = WindowBuilder::new()
        .with_inner_size(PhysicalSize {
            width: 1440,
            height: 900,
        })
        .with_resizable(false)
        .build_vk_surface(&event_loop, instance.clone())
        .unwrap();
    (physical, event_loop, surface)
}

pub fn make_device(
    physical: PhysicalDevice,
    surface: &Arc<Surface<Window>>,
) -> (Arc<Device>, Arc<Queue>) {
    let queue_family = physical
        .queue_families()
        .find(|&q| q.supports_graphics() && surface.is_supported(q).unwrap_or(false))
        .unwrap();
    let device_ext = DeviceExtensions {
        khr_swapchain: true,
        ..DeviceExtensions::none()
    };
    let (device, mut queues) = Device::new(
        physical,
        physical.supported_features(),
        &device_ext,
        [(queue_family, 0.5)].iter().cloned(),
    )
    .unwrap();
    (device, queues.next().unwrap())
}

pub type ArcAnyImage = Arc<dyn ImageAccess + Send + Sync>;
pub type ArcAnyImageView = Arc<dyn ImageAccess + Send + Sync>;

make_wd_fn!(
    make_solid_pipeline,
    shaders::solid::vs::Shader,
    shaders::solid::fs::Shader,
    OneVertexOneInstanceDefinition::<objects::Vertex, objects::Instance>
);
make_wd_fn!(
    make_shadow_pipeline,
    shaders::shadow::vs::Shader,
    shaders::shadow::fs::Shader,
    OneVertexOneInstanceDefinition::<objects::Vertex, objects::Instance>
);
make_wd_fn!(
    make_debug_pipeline,
    shaders::debug::vs::Shader,
    shaders::debug::fs::Shader,
    SingleBufferDefinition::<objects::SimpleVertex>
);

pub fn make_render_pass(device: &Arc<Device>, format: Format) -> Arc<RenderPass> {
    Arc::new(
        vulkano::single_pass_renderpass!(device.clone(),
            attachments: {
                color: {
                    load: Clear,
                    store: Store,
                    format: format,
                    samples: 1,
                },
                depth: {
                    load: Clear,
                    store: DontCare,
                    format: Format::D16Unorm,
                    samples: 1,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {depth},
                resolve: [],
            }
        )
        .unwrap(),
    )
}
