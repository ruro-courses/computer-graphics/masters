pub mod debug;
pub mod shadow;
pub mod solid;

#[macro_export]
macro_rules! make_wd_fn {
    ( $fname:ident, $vs_shader:ty, $fs_shader:ty, $vertex_input:ty ) => {
        pub fn $fname(
            device: Arc<Device>,
            vs: &$vs_shader,
            fs: &$fs_shader,
            images: Vec<Arc<dyn ImageAccess + Send + Sync>>,
            depth_buffer: Arc<dyn ImageAccess + Send + Sync>,
            render_pass: Arc<RenderPass>,
            dimensions: [u32; 2],
        ) -> (
            Arc<dyn GraphicsPipelineAbstract + Send + Sync>,
            Vec<Arc<dyn FramebufferAbstract + Send + Sync>>,
        ) {
            let framebuffers = images
                .iter()
                .map(|image| {
                    Arc::new(
                        Framebuffer::start(render_pass.clone())
                            .add(ImageView::new(image.clone()).unwrap())
                            .unwrap()
                            .add(ImageView::new(depth_buffer.clone()).unwrap())
                            .unwrap()
                            .build()
                            .unwrap(),
                    ) as Arc<dyn FramebufferAbstract + Send + Sync>
                })
                .collect::<Vec<_>>();

            let pipeline = Arc::new(
                GraphicsPipeline::start()
                    .vertex_input(<$vertex_input>::new())
                    .vertex_shader(vs.main_entry_point(), ())
                    .triangle_list()
                    .viewports_dynamic_scissors_irrelevant(1)
                    .viewports(once(Viewport {
                        origin: [0.0, 0.0],
                        dimensions: [dimensions[0] as f32, dimensions[1] as f32],
                        depth_range: 0.0..1.0,
                    }))
                    .fragment_shader(fs.main_entry_point(), ())
                    .depth_stencil_simple_depth()
                    .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
                    .build(device.clone())
                    .unwrap(),
            );

            (pipeline, framebuffers)
        }
    };
}
