// CONSTANTS
const vec3 LIGHT = vec3(1.0, 1.0, 1.0);
const float SHININESS = 4.0;

// UNIFORMS
layout(set = 0, binding = 0) uniform Data {
    mat4 view;
    mat4 proj;
    vec4 light_source;
} uniforms;

vec3 view_local_light_source() {
    vec4 src = uniforms.view * vec4(uniforms.light_source.xyz, 1.0);
    src /= src.w;
    return src.xyz;
}
