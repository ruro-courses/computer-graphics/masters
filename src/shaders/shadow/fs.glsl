#version 460
#include "../_common.glsl"

// IN/OUT
layout(location = 0) in float v_dist;
layout(location = 0) out float f_color;

// CODE
void main(void)
{
    f_color = v_dist;
}
