pub mod vs {
    vulkano_shaders::shader! {
        ty: "vertex",
        path: "src/shaders/shadow/vs.glsl",
    }
}

pub mod fs {
    vulkano_shaders::shader! {
        ty: "fragment",
        path: "src/shaders/shadow/fs.glsl",
    }
}
