#version 460
#include "../_common.glsl"

// IN/OUT
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uvs;
layout(location = 3) in mat4 transform;

layout(location = 0) out vec3 v_position;
layout(location = 1) out vec3 v_normal;
layout(location = 2) out vec2 v_uvs;

void main() {
    vec4 pos = transform * vec4(position, 1.0);
    pos /= pos.w;
    pos = uniforms.view * pos;
    pos /= pos.w;

    gl_Position = uniforms.proj * pos;
    v_position = pos.xyz;
    v_normal = mat3(uniforms.view * transform) * normal;
    v_uvs = uvs;
}
