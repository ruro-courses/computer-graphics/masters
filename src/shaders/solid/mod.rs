pub mod vs {
    vulkano_shaders::shader! {
        ty: "vertex",
        path: "src/shaders/solid/vs.glsl",
    }
}

pub mod fs {
    vulkano_shaders::shader! {
        ty: "fragment",
        path: "src/shaders/solid/fs.glsl",
    }
}
