#version 460
#include "../_common.glsl"

// IN/OUT
layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_uvs;

layout(location = 0) out vec4 f_color;

// TEXTURES
layout(set = 0, binding = 1) uniform sampler2D diffuse;
layout(set = 0, binding = 2) uniform samplerCube shadow;

// CODE
vec3 LightPos(
in vec3 Fpos//, in int lightIndex
)
{
    return view_local_light_source() - Fpos;
    // Light -1 is the sun at inf, so it doesn't get translated
    // if (lightIndex == -1) { return U_sunPos; }
    // return L[lightIndex].pos.xyz - Fpos;
}

vec3 LightCol(
in vec3 Lpos//, in int lightIndex
)
{
    return (uniforms.light_source.a / dot(Lpos, Lpos)) * vec3(1.0, 1.0, 1.0);
    // Light -1 is the sun at inf, so it's always the same brightness
    // if (lightIndex == -1) { return vec3(1, 1, 1); }
    // return (L[lightIndex].col.a / dot(Lpos, Lpos)) * L[lightIndex].col.rgb;
}


vec3 Illuminate(
in vec3 Famb,
in vec3 Fdif,
in vec3 Fspe,
in vec3 Fpos,
in vec3 Ndir,
in float Sval
)
{
    vec3 total = Famb;

    // for (int li = -1; li < L.length(); ++li)
    // {
    const vec3 Lpos = LightPos(Fpos);//, li);
    const vec3 Ldir = normalize(Lpos);
    const float Idif = dot(Ndir, Ldir);
    if (Idif > 0)
    {
        const vec3 Lcol = LightCol(Lpos);//, li);
        const vec3 Vdir = normalize(Fpos);
        const vec3 Hdir = normalize(Ldir + Vdir);
        float Ispe = dot(Ndir, Hdir);
        Ispe = (Ispe > 0) ? pow(Ispe, SHININESS) : 0;

        total.rgb += Sval * Lcol * (Idif * Fdif.rgb + Ispe * Fspe.rgb);
    }
    // }

    return clamp(total, 0, 1);
}


void main(void)
{
    // vec3 Famb = texture(U_Tambient, v_uvs).rgb;
    vec3 Fdif = texture(diffuse, v_uvs).rgb;
    //vec3 Fspe = texture(U_Tspecular, v_uvs).rgb;
    // Famb *= U_Cambient;
    // Fdif *= U_Cdiffuse;
    // Fspe *= U_Cspecular;

    vec4 relative_light_pos = inverse(uniforms.view) * vec4(v_position, 1.0);
    relative_light_pos /= relative_light_pos.w;
    relative_light_pos.xyz -= uniforms.light_source.xyz;

    float shadow_dist = texture(shadow, normalize(relative_light_pos.xyz)).x;
    float actual_dist = length(relative_light_pos.xyz);
    float shadow_mult = float(shadow_dist + 0.01 > actual_dist);

    vec3 ill = Illuminate(
        0.2 * Fdif, // Famb,
        Fdif,
        vec3(0.1, 0.1, 0.1), // Fspe,
        v_position,
        normalize(v_normal),
        shadow_mult
    );

    f_color = vec4(ill, 1.0);
}
