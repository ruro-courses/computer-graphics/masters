#version 460
#include "../_common.glsl"

// IN/OUT
layout(location = 0) in vec3 position;
layout(location = 0) out vec3 v_position;

void main() {
    v_position = (position + 1.0)/2.0;
    v_position.x *= 4;
    v_position.y *= 3;
    gl_Position = vec4(position, 1.0);
}
