#version 460
#include "../_common.glsl"

// IN/OUT
layout(location = 0) in vec3 v_position;
layout(location = 0) out vec4 f_color;

// TEXTURES
layout(set = 0, binding = 1) uniform samplerCube shadow;

// CODE
vec4 get_shadow_debug(float u, float v, float w) {
    return vec4(texture(shadow, vec3(u, v, w)).xxx / 4.0, 1.0);
}

void main(void)
{
    int xface = int(v_position.x);
    int yface = int(v_position.y);
    if (yface == 1) {
        vec2 uv = 2 * vec2(v_position.x - xface, v_position.y - 1) - 1;
        switch (xface) {
            case 0: f_color = get_shadow_debug(-1.0f, uv.y, uv.x); break;// NEGATIVE_X
            case 1: f_color = get_shadow_debug(uv.x, uv.y, 1.0f); break;// POSITIVE_Z
            case 2: f_color = get_shadow_debug(1.0, uv.y, -uv.x); break;// POSITIVE_X
            case 3: f_color = get_shadow_debug(-uv.x, uv.y, -1.0f); break;// NEGATIVE_Z
            default : f_color = vec4(1.0, 0.0, 0.0, 1.0); break;// ???
        }
    } else {
        if (xface == 1) {
            vec2 uv = 2 * vec2(v_position.x - 1, v_position.y - yface) - 1;
            switch (yface) {
                case 0: f_color = get_shadow_debug(uv.x, -1.0f, uv.y); break;// NEGATIVE_Y
                case 2: f_color = get_shadow_debug(uv.x, 1.0f, -uv.y); break;// POSITIVE_Y
                default : f_color = vec4(0.0, 1.0, 0.0, 1.0); break;// ???
            }
        } else {
            f_color = vec4(0.0, 0.0, 1.0, 1.0);// ???
        }
    }

}
