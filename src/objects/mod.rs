use std::io::Cursor;
use std::sync::Arc;

use serde::Deserialize;
use vulkano::command_buffer::{CommandBufferExecFuture, PrimaryAutoCommandBuffer};
use vulkano::device::{Device, Queue};
use vulkano::format::Format;
use vulkano::image::{ImageDimensions, ImmutableImage, MipmapsCount};
use vulkano::sampler::{Filter, MipmapMode, Sampler, SamplerAddressMode};
use vulkano::sync::NowFuture;

#[derive(Default, Deserialize, Copy, Clone, Debug)]
pub struct Vertex {
    pub position: (f32, f32, f32),
    pub normal: (f32, f32, f32),
    pub uvs: (f32, f32),
}

#[derive(Default, Deserialize, Copy, Clone, Debug)]
pub struct SimpleVertex {
    pub position: (f32, f32, f32),
}

#[derive(Default, Deserialize, Copy, Debug, Clone)]
pub struct Instance {
    pub transform: [[f32; 4]; 4],
}

vulkano::impl_vertex!(Vertex, position, normal, uvs);
vulkano::impl_vertex!(SimpleVertex, position);
vulkano::impl_vertex!(Instance, transform);

#[macro_export]
macro_rules! load_simple_object {
    ( $device:ident, $x:expr ) => {{
        let vertices = serde_json::from_str::<Vec<objects::SimpleVertex>>(include_str!(concat!(
            "objects/", $x, ".json"
        )))
        .unwrap();
        CpuAccessibleBuffer::from_iter(
            $device.clone(),
            BufferUsage::all(),
            false,
            vertices.iter().cloned(),
        )
        .unwrap()
    }};
}

#[macro_export]
macro_rules! load_object {
    ( $device:ident, $x:expr ) => {{
        let (vertices, indices) = serde_json::from_str::<(Vec<objects::Vertex>, Vec<u16>)>(
            include_str!(concat!("objects/", $x, ".json")),
        )
        .unwrap();
        let vertex_buffer = CpuAccessibleBuffer::from_iter(
            $device.clone(),
            BufferUsage::all(),
            false,
            vertices.iter().cloned(),
        )
        .unwrap();
        let index_buffer = CpuAccessibleBuffer::from_iter(
            $device.clone(),
            BufferUsage::all(),
            false,
            indices.iter().cloned(),
        )
        .unwrap();
        (vertex_buffer, index_buffer)
    }};
}

pub fn load_texture(
    queue: &Arc<Queue>,
    png_bytes: &[u8],
) -> (
    Arc<ImmutableImage>,
    CommandBufferExecFuture<NowFuture, PrimaryAutoCommandBuffer>,
) {
    let png_bytes = png_bytes.to_vec();
    let cursor = Cursor::new(png_bytes);
    let decoder = png::Decoder::new(cursor);
    let (info, mut reader) = decoder.read_info().unwrap();
    let dimensions = ImageDimensions::Dim2d {
        width: info.width,
        height: info.height,
        array_layers: 1,
    };
    let mut image_data = Vec::new();
    image_data.resize((info.width * info.height * 4) as usize, 0);
    reader.next_frame(&mut image_data).unwrap();

    ImmutableImage::from_iter(
        image_data.iter().cloned(),
        dimensions,
        MipmapsCount::Log2,
        Format::R8G8B8A8Srgb,
        queue.clone(),
    )
    .unwrap()
}

pub fn make_sampler(device: &Arc<Device>, _mip_levels: u32) -> Arc<Sampler> {
    Sampler::new(
        device.clone(),
        Filter::Linear,
        Filter::Linear,
        MipmapMode::Linear,
        SamplerAddressMode::Repeat,
        SamplerAddressMode::Repeat,
        SamplerAddressMode::Repeat,
        0.0,
        1.0,
        0.0,
        0.0, // mip_levels.into(),
    )
    .unwrap()
}
