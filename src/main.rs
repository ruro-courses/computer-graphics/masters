use std::convert::TryInto;
use std::iter::once;
use std::sync::Arc;
use std::time::Instant;

use cgmath::{Deg, Matrix4, Point3, Rad, Vector3, Vector4};
use itertools::izip;
use vulkano::buffer::cpu_pool::{CpuBufferPool, CpuBufferPoolSubbuffer};
use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer};
use vulkano::command_buffer::{
    AutoCommandBufferBuilder, CommandBufferUsage, DynamicState, SubpassContents,
};
use vulkano::descriptor::descriptor_set::{PersistentDescriptorSet, PersistentDescriptorSetBuf};
use vulkano::format::Format;
use vulkano::image::view::{ImageView, ImageViewType};
use vulkano::image::{
    AttachmentImage, ImageAccess, ImageCreateFlags, ImageDimensions, ImageUsage, StorageImage,
};
use vulkano::instance::debug::DebugCallback;
use vulkano::instance::{Instance, InstanceExtensions};
use vulkano::memory::pool::StdMemoryPool;
use vulkano::swapchain;
use vulkano::sync;
use vulkano::sync::GpuFuture;
use winit::event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent};
use winit::event_loop::ControlFlow;

use boilerplate::ArcAnyImageView;

use crate::boilerplate::ArcAnyImage;

mod boilerplate;
mod objects;
mod shaders;

fn main() {
    println!("Initializing persistent data. This might take a while.");
    // region Instance, device, swapchain setup
    let required_extensions = InstanceExtensions {
        ext_debug_utils: true,
        ..vulkano_win::required_extensions()
    };
    let instance = Instance::new(None, &required_extensions, None).unwrap();
    let _callback = DebugCallback::errors_and_warnings(&instance, |msg| {
        println!("Debug callback: {:?}", msg.description);
    })
    .ok()
    .unwrap();

    let (physical, event_loop, surface) = boilerplate::make_physical(&instance);
    let dimensions: [u32; 2] = surface.window().inner_size().into();
    let (device, queue) = boilerplate::make_device(physical, &surface);
    let (swapchain, images) =
        boilerplate::make_swapchain(physical, &surface, &device, queue.clone());
    // endregion

    // region Prepare data
    // region Prepare box data
    println!("Loading box.");
    let (box_vertex_buffer, box_index_buffer) = load_object!(device, "box_full");
    let box_instance_buffer =
        CpuBufferPool::<objects::Instance>::new(device.clone(), BufferUsage::all());
    let (box_texture, box_tex_future) =
        objects::load_texture(&queue, include_bytes!("objects/wood2_small.png"));
    let box_texture_sampler = objects::make_sampler(&device, box_texture.mipmap_levels());
    // endregion
    // region Prepare ground data
    println!("Loading ground.");
    let (ground_vertex_buffer, ground_index_buffer) = load_object!(device, "ground");
    let ground_instance_buffer =
        CpuBufferPool::<objects::Instance>::new(device.clone(), BufferUsage::all());
    let (ground_texture, ground_tex_future) =
        objects::load_texture(&queue, include_bytes!("objects/ground.png"));
    let ground_texture_sampler = objects::make_sampler(&device, ground_texture.mipmap_levels());
    // endregion
    // region Prepare amogus data
    println!("Loading amogus.");
    let (amogus_vertex_buffer, amogus_index_buffer) = load_object!(device, "amogus");
    let amogus_instance_buffer =
        CpuBufferPool::<objects::Instance>::new(device.clone(), BufferUsage::all());
    let (amogus_texture, amogus_tex_future) =
        objects::load_texture(&queue, include_bytes!("objects/amogus.png"));
    let amogus_texture_sampler = objects::make_sampler(&device, amogus_texture.mipmap_levels());
    // endregion
    // region Prepare shadow map data
    println!("Preparing shadow map.");
    let cubemap_size: u32 = 2048;
    let cubemap_dimensions = [cubemap_size, cubemap_size];
    let shadow_sampler = objects::make_sampler(&device, 1);
    let shadow_cubemap_buffer = StorageImage::with_usage(
        device.clone(),
        ImageDimensions::Dim2d {
            width: cubemap_size,
            height: cubemap_size,
            array_layers: 6,
        },
        Format::R32Sfloat,
        ImageUsage {
            sampled: true,
            transfer_source: false,
            transfer_destination: true,
            storage: true,
            color_attachment: true,
            depth_stencil_attachment: false,
            transient_attachment: false,
            input_attachment: true,
        },
        ImageCreateFlags {
            cube_compatible: true,
            ..ImageCreateFlags::default()
        },
        once(queue.family()),
    )
    .unwrap();
    let shadow_buffers: Vec<Arc<StorageImage>> = (0..6)
        .map(|_| {
            StorageImage::with_usage(
                device.clone(),
                ImageDimensions::Dim2d {
                    width: cubemap_size,
                    height: cubemap_size,
                    array_layers: 1,
                },
                shadow_cubemap_buffer.format(),
                ImageUsage {
                    sampled: true,
                    transfer_source: true,
                    transfer_destination: true,
                    storage: true,
                    color_attachment: true,
                    depth_stencil_attachment: false,
                    transient_attachment: false,
                    input_attachment: true,
                },
                ImageCreateFlags::default(),
                once(queue.family()),
            )
            .unwrap()
        })
        .collect();
    let shadow_buffer_views: Vec<ArcAnyImageView> = shadow_buffers
        .iter()
        .map(|x| x.clone() as ArcAnyImageView)
        .collect();
    let shadow_buffers: Vec<ArcAnyImage> = shadow_buffers
        .iter()
        .map(|x| x.clone() as ArcAnyImage)
        .collect();
    // endregion
    // region Prepare misc/debug data
    println!("Preparing debug buffers.");
    let screen_vertex_buffer = load_simple_object!(device, "screen");
    let uniform_buffer =
        CpuBufferPool::<shaders::solid::vs::ty::Data>::new(device.clone(), BufferUsage::all());
    let depth_buffer =
        AttachmentImage::transient(device.clone(), dimensions, Format::D16Unorm).unwrap();
    let shadow_depth_buffer =
        AttachmentImage::transient(device.clone(), cubemap_dimensions, Format::D16Unorm).unwrap();
    // endregion
    // endregion

    // region Prepare pipelines
    println!("Building pipelines.");
    // region Prepare shadow pipeline
    let (shadow_pipeline, shadow_framebuffers) = {
        boilerplate::make_shadow_pipeline(
            device.clone(),
            &shaders::shadow::vs::Shader::load(device.clone()).unwrap(),
            &shaders::shadow::fs::Shader::load(device.clone()).unwrap(),
            shadow_buffer_views,
            shadow_depth_buffer.clone(),
            boilerplate::make_render_pass(&device, Format::R32Sfloat),
            cubemap_dimensions,
        )
    };
    // endregion
    // region Prepare debug pipeline
    let (debug_pipeline, debug_framebuffers) = {
        boilerplate::make_debug_pipeline(
            device.clone(),
            &shaders::debug::vs::Shader::load(device.clone()).unwrap(),
            &shaders::debug::fs::Shader::load(device.clone()).unwrap(),
            images
                .iter()
                .map(|image| image.clone() as ArcAnyImageView)
                .collect(),
            depth_buffer.clone(),
            boilerplate::make_render_pass(&device, swapchain.format()),
            dimensions,
        )
    };
    // endregion
    // region Prepare solid pipeline
    let (solid_pipeline, solid_framebuffers) = {
        boilerplate::make_solid_pipeline(
            device.clone(),
            &shaders::solid::vs::Shader::load(device.clone()).unwrap(),
            &shaders::solid::fs::Shader::load(device.clone()).unwrap(),
            images
                .iter()
                .map(|image| image.clone() as ArcAnyImageView)
                .collect(),
            depth_buffer.clone(),
            boilerplate::make_render_pass(&device, swapchain.format()),
            dimensions,
        )
    };
    // endregion
    // endregion

    // region Event loop state
    let mut debug_mode = true;
    let mut previous_frame_end = Some(
        sync::now(device.clone())
            .join(ground_tex_future)
            .join(amogus_tex_future)
            .join(box_tex_future)
            .boxed(),
    );
    let rotation_start = Instant::now();
    // endregion

    println!("Done. Starting the event loop.");
    // region Event loop
    event_loop.run(move |event, _, control_flow| {
        match event {
            // region Window events
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                *control_flow = ControlFlow::Exit;
            }
            Event::WindowEvent {
                event:
                WindowEvent::KeyboardInput {
                    input:
                    KeyboardInput {
                        state: ElementState::Pressed,
                        virtual_keycode: Some(keycode),
                        ..
                    },
                    ..
                },
                ..
            } => match keycode {
                VirtualKeyCode::Escape => {
                    *control_flow = ControlFlow::Exit;
                }
                VirtualKeyCode::Key1 => {
                    debug_mode = false;
                }
                VirtualKeyCode::Key2 => {
                    debug_mode = true;
                }
                _ => (),
            },
            // endregion
            Event::RedrawEventsCleared => {
                // region Main redraw event
                // region Prepare swap
                previous_frame_end.as_mut().unwrap().cleanup_finished();
                let (image_num, _, acquire_future) =
                    swapchain::acquire_next_image(swapchain.clone(), None).unwrap();
                // endregion
                // region Animate via subbuffers
                let (
                    uniform_buffer_subbuffer,
                    cubemap_uniform_buffer_subbuffers,
                    box_instance_buffer_subbuffer,
                    ground_instance_buffer_subbuffer,
                    amogus_instance_buffer_subbuffer,
                ) = {
                    let elapsed = rotation_start.elapsed();
                    let animation = (elapsed.as_secs() as f64
                        + elapsed.subsec_nanos() as f64 / 1_000_000_000.0)
                        as f32;

                    let aspect_ratio = dimensions[0] as f32 / dimensions[1] as f32;
                    let proj = cgmath::perspective(
                        Rad(std::f32::consts::FRAC_PI_2),
                        aspect_ratio,
                        0.01,
                        100.0,
                    );
                    let view = Matrix4::look_at(
                        Point3::<f32>::new(
                            -0.3 - 0.5 * (0.7 * animation).cos(),
                            -0.3,
                            1.25 + 0.5 * (0.7 * animation).sin()),
                        Point3::<f32>::new(0.0, 0.0, 0.0),
                        Vector3::<f32>::new(0.0, 1.0, 0.0),
                    );
                    let light_source =
                        Vector4::<f32>::new(
                            0.5 * animation.sin(),
                            0.5 * animation.cos(),
                            2.0 * (animation / 3.0).sin(),
                            10.0 * (2.0 + (animation / 3.0).sin()),
                        );

                    let uniform_data = shaders::solid::vs::ty::Data {
                        view: view.into(),
                        proj: proj.into(),
                        light_source: light_source.into(),
                    };

                    let face_proj = cgmath::perspective(
                        Rad(std::f32::consts::FRAC_PI_2),
                        1.0,
                        0.01,
                        100.0,
                    );
                    let face_views = vec![
                        // POSITIVE_X
                        Matrix4::<f32>::from_angle_y(Deg(90.0))
                            * Matrix4::<f32>::from_angle_x(Deg(180.0)),
                        // NEGATIVE_X
                        Matrix4::<f32>::from_angle_y(Deg(-90.0))
                            * Matrix4::<f32>::from_angle_x(Deg(180.0)),
                        // POSITIVE_Y
                        Matrix4::<f32>::from_angle_x(Deg(-90.0)),
                        // NEGATIVE_Y
                        Matrix4::<f32>::from_angle_x(Deg(90.0)),
                        // POSITIVE_Z
                        Matrix4::<f32>::from_angle_x(Deg(180.0)),
                        // NEGATIVE_Z
                        Matrix4::<f32>::from_angle_z(Deg(180.0)),
                    ];

                    let cubemap_uniforms_data: Vec<shaders::solid::vs::ty::Data> =
                        face_views.into_iter().map(
                            |face_view| {
                                shaders::solid::vs::ty::Data {
                                    view: face_view.into(),
                                    proj: face_proj.into(),
                                    light_source: light_source.into(),
                                }
                            }).collect();

                    let box_instance_data = vec![
                        objects::Instance {
                            transform: (Matrix4::<f32>::from_scale(0.01)
                                * Matrix4::<f32>::from_translation(
                                50.0 * Vector3::<f32>::new(
                                    (5.0 * animation).cos(),
                                    (3.0 * animation).cos(),
                                    -2.0,
                                ),
                            ) * Matrix4::<f32>::from_angle_x(Rad(animation)))
                                .into(),
                        },
                        objects::Instance {
                            transform: (Matrix4::<f32>::from_scale(0.01)
                                * Matrix4::<f32>::from_translation(Vector3::<f32>::new(
                                0.0, 0.0, 0.0,
                            )) * Matrix4::<f32>::from_scale(
                                1.0 + 0.5 * animation.cos(),
                            ))
                                .into(),
                        },
                        objects::Instance {
                            transform: (Matrix4::<f32>::from_scale(0.01)
                                * Matrix4::<f32>::from_translation(
                                100.0 * Vector3::<f32>::new(-1.0, 0.0, 0.0),
                            ) * Matrix4::<f32>::from_angle_y(Rad(-10.0 * animation)))
                                .into(),
                        },
                        objects::Instance {
                            transform: (Matrix4::<f32>::from_scale(0.01)
                                * Matrix4::<f32>::from_translation(
                                100.0 * Vector3::<f32>::new(1.0, 0.0, 0.0),
                            ) * Matrix4::<f32>::from_angle_x(Rad(-animation)))
                                .into(),
                        },
                        objects::Instance {
                            transform: (Matrix4::<f32>::from_scale(0.01)
                                * Matrix4::<f32>::from_translation(
                                100.0 * Vector3::<f32>::new(0.0, -1.0, 0.0),
                            ) * Matrix4::<f32>::from_angle_z(Rad(-animation)))
                                .into(),
                        },
                        objects::Instance {
                            transform: (Matrix4::<f32>::from_scale(0.01)
                                * Matrix4::<f32>::from_translation(
                                100.0 * Vector3::<f32>::new(0.0, 1.0, 0.0),
                            ) * Matrix4::<f32>::from_angle_z(Rad(0.1 * animation)))
                                .into(),
                        },
                    ];

                    let ground_instance_data = vec![
                        objects::Instance {
                            transform: (Matrix4::<f32>::from_scale(0.01)
                                * Matrix4::<f32>::from_translation(
                                50.0 * Vector3::<f32>::new(
                                    0.0, 0.0, -10.0,
                                ),
                            ) * Matrix4::<f32>::from_scale(
                                10.0
                            ) * Matrix4::<f32>::from_angle_x(
                                Deg(90.0)
                            )
                            ).into(),
                        }
                    ];

                    let amogus_instance_data = vec![
                        objects::Instance {
                            transform: (
                                Matrix4::<f32>::from_scale(0.01)
                                    * Matrix4::<f32>::from_translation(
                                    50.0 * Vector3::<f32>::new(
                                        0.0,
                                        (13.0 * animation).cos().abs(),
                                        -7.5,
                                    ),
                                ) * Matrix4::<f32>::from_scale(
                                    1.0
                                ) * Matrix4::<f32>::from_angle_z(
                                    Deg(180.0)
                                ) * Matrix4::<f32>::from_angle_y(
                                    Rad(11.0 * animation)
                                ) * Matrix4::<f32>::from_translation(
                                    Vector3::<f32>::new(0.0, -100.0, 0.0),
                                )
                            ).into(),
                        }
                    ];

                    let cubemap_uniform_buffer_subbuffers:
                        Vec<CpuBufferPoolSubbuffer<
                            shaders::solid::vs::ty::Data,
                            Arc<StdMemoryPool>
                        >> =
                        cubemap_uniforms_data.into_iter().map(
                            |x| { uniform_buffer.next(x).unwrap() }
                        ).collect();

                    (
                        uniform_buffer.next(uniform_data).unwrap(),
                        cubemap_uniform_buffer_subbuffers,
                        Arc::new(box_instance_buffer.chunk(box_instance_data).unwrap()),
                        Arc::new(ground_instance_buffer.chunk(ground_instance_data).unwrap()),
                        Arc::new(amogus_instance_buffer.chunk(amogus_instance_data).unwrap()),
                    )
                };
                // endregion
                // region Create descriptor sets
                // region Shadow set
                let shadow_sets: Vec<Arc<PersistentDescriptorSet<(
                    (),
                    PersistentDescriptorSetBuf<CpuBufferPoolSubbuffer<
                        shaders::solid::vs::ty::Data,
                        Arc<StdMemoryPool>
                    >>
                )>>> =
                    cubemap_uniform_buffer_subbuffers.into_iter().map(
                        |cubemap_uniform_buffer_subbuffer| {
                            Arc::new(
                                PersistentDescriptorSet::start(
                                    shadow_pipeline.descriptor_set_layout(0).unwrap().clone(),
                                )
                                    .add_buffer(cubemap_uniform_buffer_subbuffer.clone())
                                    .unwrap()
                                    .build()
                                    .unwrap(),
                            )
                        }).collect();
                // endregion
                // region Debug set
                let debug_set = Arc::new(
                    PersistentDescriptorSet::start(
                        debug_pipeline.descriptor_set_layout(0).unwrap().clone(),
                    )
                        .add_buffer(uniform_buffer_subbuffer.clone())
                        .unwrap()
                        .add_sampled_image(
                            ImageView::start(shadow_cubemap_buffer.clone())
                                .with_type(ImageViewType::Cubemap)
                                .build()
                                .unwrap(),
                            shadow_sampler.clone(),
                        )
                        .unwrap()
                        .build()
                        .unwrap(),
                );
                // endregion
                // region Solid (ground) set
                let solid_ground_set = Arc::new(
                    PersistentDescriptorSet::start(
                        solid_pipeline.descriptor_set_layout(0).unwrap().clone(),
                    )
                        .add_buffer(uniform_buffer_subbuffer.clone())
                        .unwrap()
                        .add_sampled_image(ImageView::new(ground_texture.clone()).unwrap(),
                                           ground_texture_sampler.clone())
                        .unwrap()
                        .add_sampled_image(
                            ImageView::start(shadow_cubemap_buffer.clone())
                                .with_type(ImageViewType::Cubemap)
                                .build()
                                .unwrap(),
                            shadow_sampler.clone(),
                        )
                        .unwrap()
                        .build()
                        .unwrap(),
                );
                // endregion
                // region Solid (amogus) set
                let solid_amogus_set = Arc::new(
                    PersistentDescriptorSet::start(
                        solid_pipeline.descriptor_set_layout(0).unwrap().clone(),
                    )
                        .add_buffer(uniform_buffer_subbuffer.clone())
                        .unwrap()
                        .add_sampled_image(ImageView::new(amogus_texture.clone()).unwrap(),
                                           amogus_texture_sampler.clone())
                        .unwrap()
                        .add_sampled_image(
                            ImageView::start(shadow_cubemap_buffer.clone())
                                .with_type(ImageViewType::Cubemap)
                                .build()
                                .unwrap(),
                            shadow_sampler.clone(),
                        )
                        .unwrap()
                        .build()
                        .unwrap(),
                );
                // endregion
                // region Solid (box) set
                let solid_box_set = Arc::new(
                    PersistentDescriptorSet::start(
                        solid_pipeline.descriptor_set_layout(0).unwrap().clone(),
                    )
                        .add_buffer(uniform_buffer_subbuffer.clone())
                        .unwrap()
                        .add_sampled_image(ImageView::new(box_texture.clone()).unwrap(),
                                           box_texture_sampler.clone())
                        .unwrap()
                        .add_sampled_image(
                            ImageView::start(shadow_cubemap_buffer.clone())
                                .with_type(ImageViewType::Cubemap)
                                .build()
                                .unwrap(),
                            shadow_sampler.clone(),
                        )
                        .unwrap()
                        .build()
                        .unwrap(),
                );
                // endregion
                let solid_sets = [
                    solid_box_set.clone(),
                    solid_ground_set.clone(),
                    solid_amogus_set.clone(),
                ];
                // endregion

                // region Build the command buffer
                // region Start
                let mut builder = AutoCommandBufferBuilder::primary(
                    device.clone(),
                    queue.family(),
                    CommandBufferUsage::OneTimeSubmit,
                )
                    .unwrap();
                // endregion
                // region Render pass 1 (shadow map)

                let vertex_buffers = [
                    box_vertex_buffer.clone(),
                    ground_vertex_buffer.clone(),
                    amogus_vertex_buffer.clone(),
                ];
                let instance_buffer_subbuffers = [
                    box_instance_buffer_subbuffer.clone(),
                    ground_instance_buffer_subbuffer.clone(),
                    amogus_instance_buffer_subbuffer.clone(),
                ];
                let index_buffers = [
                    box_index_buffer.clone(),
                    ground_index_buffer.clone(),
                    amogus_index_buffer.clone(),
                ];
                for (
                    face,
                    shadow_buffer,
                    shadow_framebuffer,
                    shadow_set,
                ) in izip!(
                    (0..shadow_framebuffers.len()),
                    shadow_buffers.clone(),
                    &shadow_framebuffers,
                    &shadow_sets,
                ) {
                    builder
                        .begin_render_pass(
                            shadow_framebuffer.clone(),
                            SubpassContents::Inline,
                            vec![[0f32].into(), 1f32.into()],
                        )
                        .unwrap();
                    for (
                        vertex_buffer,
                        instance_buffer_subbuffer,
                        index_buffer,
                    ) in izip!(
                        &vertex_buffers,
                        &instance_buffer_subbuffers,
                        &index_buffers,
                    ) {
                        builder
                            .draw_indexed(
                                shadow_pipeline.clone(),
                                &DynamicState::none(),
                                vec![vertex_buffer.clone(), instance_buffer_subbuffer.clone()],
                                index_buffer.clone(),
                                shadow_set.clone(),
                                (),
                                vec![],
                            ).unwrap();
                    }
                    builder
                        .end_render_pass()
                        .unwrap();
                    builder
                        .copy_image(
                            shadow_buffer,
                            [0, 0, 0],
                            0,
                            0,
                            shadow_cubemap_buffer.clone(),
                            [0, 0, 0],
                            face.try_into().unwrap(),
                            0,
                            [cubemap_size, cubemap_size, 1],
                            1,
                        ).unwrap();
                }
                // endregion
                if debug_mode {
                    // region Render pass 2 (debug display)
                    builder
                        .begin_render_pass(
                            debug_framebuffers[image_num].clone(),
                            SubpassContents::Inline,
                            vec![[0.0, 0.0, 0.0, 1.0].into(), 1f32.into()],
                        )
                        .unwrap();
                    builder
                        .draw(
                            debug_pipeline.clone(),
                            &DynamicState::none(),
                            vec![screen_vertex_buffer.clone()],
                            debug_set.clone(),
                            (),
                            vec![],
                        )
                        .unwrap();
                    builder
                        .end_render_pass()
                        .unwrap();
                    // endregion
                } else {
                    // region Render pass 2 (solid objects)
                    builder
                        .begin_render_pass(
                            solid_framebuffers[image_num].clone(),
                            SubpassContents::Inline,
                            vec![[0.0, 0.0, 0.0, 1.0].into(), 1f32.into()],
                        )
                        .unwrap();
                    for (
                        vertex_buffer,
                        instance_buffer_subbuffer,
                        index_buffer,
                        solid_set,
                    ) in izip!(
                        &vertex_buffers,
                        &instance_buffer_subbuffers,
                        &index_buffers,
                        &solid_sets,
                    ) {
                        builder
                            .draw_indexed(
                                solid_pipeline.clone(),
                                &DynamicState::none(),
                                vec![vertex_buffer.clone(), instance_buffer_subbuffer.clone()],
                                index_buffer.clone(),
                                solid_set.clone(),
                                (),
                                vec![],
                            )
                            .unwrap();
                    }
                    builder
                        .end_render_pass()
                        .unwrap();
                    // endregion
                };
                let command_buffer = builder
                    .build()
                    .unwrap();
                // endregion

                // region Handle futures/next frame
                let future = previous_frame_end
                    .take()
                    .unwrap()
                    .join(acquire_future)
                    .then_execute(queue.clone(), command_buffer)
                    .unwrap()
                    .then_swapchain_present(queue.clone(), swapchain.clone(), image_num)
                    .then_signal_fence_and_flush();
                match future {
                    Ok(future) => {
                        previous_frame_end = Some(future.boxed());
                    }
                    Err(e) => {
                        println!("Failed to flush future: {:?}", e);
                        previous_frame_end = Some(sync::now(device.clone()).boxed());
                    }
                }
                // endregion
                // endregion
            }
            _ => (),
        }
    });
    // endregion
}
