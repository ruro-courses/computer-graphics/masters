# Shadow Mapping with Vulkan and Rust

## Features:

|                                 Feature                                  |    |                          Notes / How to enable                           | Points |
|:------------------------------------------------------------------------:|:---|:------------------------------------------------------------------------:|:------:|
|                       Обязательная часть (база #1)                       |    |                                   ---                                    |  +10   |
|                       Обязательная часть (база #2)                       |    |                                   ---                                    |  +10   |
| Тени от источника во все стороны (при помощи кубических текстурных карт) |    |                                   ---                                    |   +4   |
|       **Далее - субъективные бонусы, на усмотрение проверяющего**        |    |                                                                          |        |
|               Реалистичные движения (анимация персонажей)                |    | Формально анимация персонажа есть, набирает ли она на балл - не знаю :^) |  +0-4  |
|                  Не предусмотренный бонус проверяющего                   |    |      Использовал Vulkan+Rust, что сложнее/интереснее чем OpenGL+C++      |  +0-5  |
|                   Бонус за приятный внешний вид сцены                    |    |                                   ---                                    |  +0-2  |

Total: 24-35 points

## Build and run:

Requrements:
- `Cargo` (tested on version `cargo 1.52.0`, can be installed/updated
  with `rustup`, any fairly recent version should work)

```bash
cargo build
cargo run
```

Note: Due to a
[known bug](https://github.com/vulkano-rs/vulkano/issues/1560) in
`vulkano`, you can get a `thread 'main' panicked at 'explicit panic'`
error when trying to run this app. This is not an error in my
application, but an implementation error in `vulkano`. Try restarting
the application a few times, it will eventually work.

## Screenshots/Video

#### Video

[Youtube Video](https://www.youtube.com/watch?v=139vFwwcy6o)

#### Render

![Render](screen01.jpg)

------------------------------

#### Shadow map debug

![Shadow map debug](screen02.jpg)

------------------------------

